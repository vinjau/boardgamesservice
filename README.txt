=================DODAVANJE DEPENDENCY===================
dodaje se u root/build.sbt
format: libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.27"

posle dodavanja se izvrsavaju sledece sbt komande

1. reload
2. update
3. compile
4. run (uzivaj)
========================================================

=================DODAVANJE VIEW-a=======================
da bi moglo da se ide views.html.MOJVIEW_BLA_BLA.render();
mora da se kuca:

// ne mora, moze samo compile
sbt eclipse komanda (refresh projekta posle u eclipse da shvati sve)
========================================================

=================UPUTSTVO ZA EBEAN======================

Entited extenduje Model
U slucaju da nece da radi modifikator polja promeniti na public
posle dodavanje nekog entiteta uraditi
sbt compile, a posle toga
pokrenuti MainQueryBeanGenerator.java (generator pravi typeSafe query klase na osnovu kompajliranih klasa)

VAZNO!!! generator negenerise polja za @Enumerated anotirana polja iz modela. Mora rucno da se doda.

Ovo omogucava Type Safe Query 

========================================================

=================UPUTSTVO DEBUG=========================


// trenutna opcija | pokrenuti pre komande sb u konzoli
set SBT_OPTS=-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=9999


========================================================