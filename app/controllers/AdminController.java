package controllers;

import models.User;
import models.helpers.UserStatus;
import models.helpers.UserTypeEnum;
import models.query.QUserType;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

public class AdminController extends Controller {

	@Security.Authenticated(Secured.class)
	public Result createMod() {
		if (Secured.isAdmin(ctx())) {
			return ok(views.html.modCreate.render());
		}
		return unauthorized("ooops nisi admin");
	}
	
	@Security.Authenticated(Secured.class)
	public Result makeMod() {
		if (Secured.isAdmin(ctx())) {
			String username = request().body().asFormUrlEncoded().get(AuthController.USERNAME)[0];
			String password = request().body().asFormUrlEncoded().get(AuthController.PASSWORD)[0];
			User user = new User();
			user.setUsername(username);
			user.setPassword(password); //DODAJ HESIRANJE
			user.setStatus(UserStatus.ACTIVE);
			user.setUserType(new QUserType().where().type.eq(UserTypeEnum.MODERATOR.toString()).findUnique());
			user.insert();
			return redirect(routes.AuthController.home());
		}
		return unauthorized("ooops nisi admin");
	}
}
