package controllers;

import org.apache.commons.codec.digest.DigestUtils;

import models.User;
import models.helpers.UserStatus;
import models.helpers.UserTypeEnum;
import models.query.QUser;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

public class AuthController extends Controller {

	public static String USERNAME = "username";
	public static String PASSWORD = "password";
	public static String AUTH_LEVEL = "level"; 
	
	public Result login() {
		if (Secured.isLoggedIn(ctx())) {
			return redirect(routes.AuthController.home());
		}
		return ok(views.html.login.render());
	}
	
	public Result logout() {
		session().clear();
		return redirect(routes.AuthController.login());
	}
	
	public Result makeLogin() {
		String username = request().body().asFormUrlEncoded().get(USERNAME)[0];
		String password = request().body().asFormUrlEncoded().get(PASSWORD)[0];
		
		// hesiranje lozinke
		password = DigestUtils.sha256Hex(password);
		
		Logger.info(username + ":" + password);
		User user = new QUser().fetch("userTypeId")
				.where()
				.username.eq(username)
				.and().password.eq(password)
				.and().status.eq(UserStatus.ACTIVE.toString())
				.and().userTypeId.type.in(UserTypeEnum.ADMIN.toString(), UserTypeEnum.MODERATOR.toString())
				.findUnique();
		if (user != null) {
			user.setPassword(null);
			Logger.info("password:" + user.getPassword());
			Logger.info(Json.toJson(user).toString());
		}
		session().clear();
		if (user != null) {
			session(USERNAME, username);
			session(AUTH_LEVEL, user.getUserType().getType().toString());
			return redirect(routes.AuthController.home());
		} else {
			return ok("Losi podaci");
		}
	}
	
	@Security.Authenticated(Secured.class)
	public Result home() {
		return ok(views.html.home.render(Secured.getUser(ctx()), Secured.getAuthLevel(ctx())));
	}
	
	public void createModerator() {
		
	}
}
