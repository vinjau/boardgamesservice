package controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.io.FileUtils;

import models.Game;
import models.GameMedia;
import models.User;
import models.helpers.GameStatus;
import models.helpers.MediaType;
import models.helpers.UserTypeEnum;
import models.query.QUser;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Result;
import play.mvc.Security;

public class ModeratorController extends Controller {
	
	private static final String USERNAME = "username";
	private static final String EMAIL = "email";
	private static final String STATUS = "status";
	private static final String POSITIVE_FROM = "positiveFrom";
	private static final String POSITIVE_TO = "positiveTo";
	private static final String NEGATIVE_FROM = "negativeFrom";
	private static final String NEGATIVE_TO = "negativeTo";
	
	@Security.Authenticated(Secured.class)
	public Result searchUsers() {
		return ok(views.html.searchUsers.render(null, null, null, null, null, null, null, null));	
	}
	
	@Security.Authenticated(Secured.class)
	public Result users() {
		try {
			String username = request().getQueryString(USERNAME);
			String email = request().getQueryString(EMAIL);
			String status = request().getQueryString(STATUS);
			String positiveFrom = request().getQueryString(POSITIVE_FROM);
			String positiveTo = request().getQueryString(POSITIVE_TO);
			String negativeFrom = request().getQueryString(NEGATIVE_FROM);
			String negativeTo = request().getQueryString(NEGATIVE_TO);
			
			Logger.info(username + " " +email + " " + status + " " + positiveFrom + " " + positiveTo + " " + negativeFrom + " " + negativeTo);
			
			QUser userQuery = new QUser();
			if (username != null && !"".equals(username)) {
				userQuery.username.ilike(username + "%");
			}
			if (email != null && !"".equals(email)) {
				userQuery.email.ilike("%" + email + "%");
			}
			if (status != null && !"".equals(status)) {
				userQuery.status.eq(status);
			}
			if (positiveFrom != null && !"".equals(positiveFrom)) {
				userQuery.positiveVotes.ge(Integer.parseInt(positiveFrom));
			}
			if (positiveTo != null  && !"".equals(positiveTo)) {
				userQuery.positiveVotes.le(Integer.parseInt(positiveTo));
			}
			if (negativeFrom != null && !"".equals(negativeFrom)) {
				userQuery.negativeVotes.ge(Integer.parseInt(negativeFrom));
			}
			if (negativeTo != null  && !"".equals(negativeTo)) {
				userQuery.negativeVotes.le(Integer.parseInt(negativeTo));
			}
			if (Secured.isModerator(ctx())) {
				userQuery.userTypeId.type.eq(UserTypeEnum.USER.toString());
			}
			List<User> users = userQuery.findList();
			return ok(views.html.searchUsers.render(users, username, email, status, positiveFrom, positiveTo, negativeFrom, negativeTo));
		} catch (Exception e) {
			return badRequest("Broj pozitivnih i negativnih nisu celi brojevi");
		}
	}
	
	@Security.Authenticated(Secured.class)
	public Result getUploadForm() {
		return ok(views.html.upload.render());
	}
	
	public Result upload() {
		String name = request().body().asMultipartFormData().asFormUrlEncoded().get("name")[0];
		String description = request().body().asMultipartFormData().asFormUrlEncoded().get("description")[0];
		String minPlayersString = request().body().asMultipartFormData().asFormUrlEncoded().get("minPlayers")[0];
		String maxPlayersString = request().body().asMultipartFormData().asFormUrlEncoded().get("maxPlayers")[0];
		Integer minPlayers = null;
		Integer maxPlayers = null;
		try {
			minPlayers = Integer.parseInt(minPlayersString);
			maxPlayers = Integer.parseInt(maxPlayersString);
		} catch (Exception e) {
		}
		Game game = new Game();
		game.setName(name);
		game.setStatus(GameStatus.ADDED);
		game.setDescription(description);
		if (minPlayers != null) {
			game.setMinPlayers(minPlayers);
		}
		if (maxPlayers != null) {
			game.setMaxPlayers(maxPlayers);
		}
	    MultipartFormData<File> body = request().body().asMultipartFormData();
	    MultipartFormData.FilePart<File> picture = body.getFile("picture");
	    game.insert();
	    if (picture != null) {
	        String fileName = picture.getFilename();
	        String contentType = picture.getContentType();
	        File imageFile = picture.getFile();
	        
	        File currentFolder = new File(Paths.get("").toAbsolutePath().toString());
	        boolean hasImageFolder = false;
	        for (File file : currentFolder.listFiles()) {
	        	if (file.getName().equals("images") && file.isDirectory()) {
	        		hasImageFolder = true;
	        		break;
	        	}
	        }
	        
	        if (!hasImageFolder) {
	        	new File(currentFolder.getAbsolutePath() + "/images").mkdir();
	        }
	        String imageName = "images/" + System.currentTimeMillis() + fileName;
	        File imageDestFile = new File(imageName);
	        
	        try {
				FileUtils.copyFile(imageFile, imageDestFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
	        GameMedia gameMedia = new GameMedia();
	        gameMedia.setGame(game);
	        gameMedia.setType(MediaType.PICTURE);
	        gameMedia.setMedia("/" + imageName);
	        gameMedia.insert();
	        return ok(imageDestFile.getAbsolutePath());
	    } else {
	        flash("error", "Missing file");
	        return badRequest();
	    }
	}
	
	public Result getImage(String url) {
		return ok(new File("images/" + url));
	}
}
