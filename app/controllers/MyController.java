package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.persistence.PersistenceException;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import controllers.Assets.Asset;
import io.ebean.Ebean;
import models.Game;
import models.GameMedia;
import models.Message;
import models.User;
import models.UserType;
import models.helpers.GameStatus;
import models.helpers.MediaType;
import models.helpers.UserTypeEnum;
import models.query.QMessage;
import models.query.QUser;
import models.query.QUserType;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.forma;
import views.html.list;
import play.libs.Json;

public class MyController extends Controller {

	public Result testo(int id) {
		return ok(Integer.toString(id));
	}
	
	public Result testo2(String id) {
		String cyka = request().getQueryString("blin");
		StringBuilder bliat = new StringBuilder();
		//Set params = request().queryString().keySet();
		
		// request().queryString().values().forEach(value -> bliat.append(value[0] + " "));
		request().queryString().keySet().forEach(
				key -> bliat.append(key + "=" + request().queryString().get(key)[0] + " ")
				);
		
		return ok(id + " " + bliat.toString());
	}
	
	public Result addChina() {
		String text = request().getQueryString("text");
		String ntext = request().getQueryString("ntext");
		return redirect("/");
	}
	
	public Result form() {
		return ok(forma.render());
	}
	
	public Result listAll() throws IOException {
		Logger.info("Called listAll method");
		/*List<China> chinalist = China.find.all();
		Bliat b = new Bliat();
		b.setNameOfBliat(" " + System.currentTimeMillis());
		Cyka c = new Cyka();
		c.setCykaField(" " + System.nanoTime());
		Ebean.save(c);
		Ebean.save(b);*/
		// Query<China> chinalist = Ebean.find(China.class).where().ilike(China.TEXT, "1%").query();
		/*Game g = new Game();
		g.setName("Game of thrones");
		g.setStatus(GameStatus.PENDING);
		
		GameMedia gm = new GameMedia();
		gm.setGame(g);
		gm.setMedia("blaa");
		gm.setType(MediaType.PICTURE);
		
		Ebean.save(g);
		Ebean.save(gm);*/
		
		User u = new User();
		/*UserType ut = new UserType();
		ut.setType("ADMIR");
		ut.insert();*/
		
		u.setEmail("bla3@bliat.com");
		u.setUsername("bliat3");
		u.setPassword("123");
		u.setUserType(new QUserType().where().type.eq(UserTypeEnum.ADMIN.toString()).findUnique());
		// u.setUserType(ut);
		
		User chkUsername = new QUser().username.eq(u.getUsername()).findUnique();
		User chkEmail = new QUser().email.eq(u.getEmail()).findUnique();
		
		StringBuilder sb = new StringBuilder();
		sb.append("Duplirana vrednost");
		if (chkEmail != null) {
			sb.append(" email");
		} else if (chkUsername != null) {
			sb.append(" username + ");
		} else {
			// u.insert();
		}
		sb.append(u.getUserType().getType());
		
		Logger.info("===========================================================================");
		// Message message = new QMessage().fetch("userFromId userToId userFromId.userTypeId userToId.userTypeId").where().id.eq(1).findUnique();
		Message message = new QMessage()
				.fetch("userFromId")
				.fetch("userToId")
				.fetch("userFromId.userTypeId")
				.fetch("userToId.userTypeId")
				.where().id.eq(1).findUnique();
		Logger.info("message: " + message.getUserFromId().getUserType().getType().toString());
		Logger.info("===========================================================================");
		Logger.info("message: " + message.getUserToId().getUserType().getType().toString());
		
		/*File file = new File("public/images/bliat.file");
		Logger.info("file path:" + file.getAbsolutePath());
		FileOutputStream fos = new FileOutputStream(file);
		fos.write("Cyka".getBytes());
		fos.close();*/
		Logger.info("routes:" + routes.Assets.versioned(new Asset("cyka")));
		
		
		return ok(sb.toString());
		

		//return ok();
		//return ok(list.render(chinalist));
	}
	
	public Result api() {
/*		Bliat b = new Bliat();
		b.setNameOfBliat(" " + System.currentTimeMillis());
		b.setId(5);
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(b);
		JsonNode node = Json.parse(json);
		Bliat b1 = Json.fromJson(node, Bliat.class);*/
		return ok(Json.toJson("blin"));
		// return ok(json).withHeader("charset", "UTF-8").as("application/json");
	}
}
