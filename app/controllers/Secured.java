package controllers;

import play.mvc.Result;
import play.mvc.Security;
import models.helpers.UserTypeEnum;
import play.mvc.Http.Context;

public class Secured extends Security.Authenticator {

	@Override
	public String getUsername(Context ctx) {
		return ctx.session().get(AuthController.USERNAME);
	}
	
	@Override
	public Result onUnauthorized(Context ctx) {
		return redirect(routes.AuthController.login());
	}
	
	public static String getUser(Context ctx) {
		return ctx.session().get(AuthController.USERNAME);
	}
	
	public static String getAuthLevel(Context ctx) {
		return ctx.session().get(AuthController.AUTH_LEVEL);
	}
	
	public static boolean isLoggedIn(Context ctx) {
		return getUser(ctx) != null;
	}
	
	public static boolean isModerator(Context ctx) {
		return getAuthLevel(ctx).equals(UserTypeEnum.MODERATOR.toString());
	}
	
	public static boolean isAdmin(Context ctx) {
		return getAuthLevel(ctx).equals(UserTypeEnum.ADMIN.toString());
	}
}
