package controllers;

import models.User;
import models.helpers.UserStatus;
import models.helpers.UserTypeEnum;
import models.query.QUser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

public class UserController extends Controller {

	public static final String STATUS = "status";
	
	@Security.Authenticated(Secured.class)
	public Result user(Long id) {
		if (Secured.isAdmin(ctx()) || Secured.isModerator(ctx())) {
			User user = new QUser().id.eq(id).findUnique();
			if (
					user != null && 
					!(
							(
									user.getUserType().getType().equals(UserTypeEnum.MODERATOR) || 
									user.getUserType().getType().equals(UserTypeEnum.ADMIN)
							) 
					&& (Secured.isModerator(ctx()))
					)
				) {
				return ok(views.html.user.render(user));
			} else {
				return badRequest("oops izgleda da korisnik ne postoji");
			}
		} else {
			return ok("Nisi admin");
		}
	}
	
	@Security.Authenticated(Secured.class)
	public Result edit(Long id) {
		if (Secured.isAdmin(ctx()) || Secured.isModerator(ctx())) {
			User user = new QUser().id.eq(id).findUnique();
			if (
					user != null && 
					!(
							(
									user.getUserType().getType().equals(UserTypeEnum.MODERATOR) || 
									user.getUserType().getType().equals(UserTypeEnum.ADMIN)
							) 
					&& (Secured.isModerator(ctx()))
					)
				) {
				return ok(views.html.userEdit.render(user));
			} else {
				return badRequest("oops izgleda da korisnik ne postoji");
			}
		} else {
			return ok("Nisi admin");
		}
	}
	
	@Security.Authenticated(Secured.class)
	public Result save(Long id) {
		if (Secured.isAdmin(ctx()) || Secured.isModerator(ctx())) {
			User user = new QUser().id.eq(id).findUnique();
			if (
					user != null && 
					!(
							(
									user.getUserType().getType().equals(UserTypeEnum.MODERATOR) || 
									user.getUserType().getType().equals(UserTypeEnum.ADMIN)
							) 
					&& (Secured.isModerator(ctx()))
					)
				) {
				String status = request().body().asFormUrlEncoded().get(STATUS)[0];
				user.setStatus(UserStatus.valueOf(status));
				user.update();
				return ok(views.html.user.render(user));
			} else {
				return badRequest("oops izgleda da korisnik ne postoji");
			}
		} else {
			return ok("Nisi admin");
		}
	}
}
