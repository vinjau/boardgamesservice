package controllers.api;

import java.util.List;

import models.Game;
import models.helpers.GameStatus;
import models.query.QGame;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

public class GameApiController extends Controller {

	public static final String NAME = "name";
	
	public Result search() {
		String name = request().getQueryString(NAME);
		if (name == null) {
			name = "";
		}
		List<Game> games = new QGame().name.ilike(name + "%").status.eq(GameStatus.ADDED.toString()).findList(); 
		return ok(Json.toJson(games));
	}
}
