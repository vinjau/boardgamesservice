package controllers.api;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

import io.ebean.Ebean;
import models.Game;
import models.GameInstance;
import models.Player;
import models.ShoutBox;
import models.User;
import models.Vote;
import models.helpers.GameInstanceStatus;
import models.helpers.GameInstanceType;
import models.helpers.GameStatus;
import models.helpers.PlayerStatus;
import models.helpers.PlayerType;
import models.helpers.UserStatus;
import models.helpers.VoteStatus;
import models.helpers.VoteType;
import models.query.QGame;
import models.query.QGameInstance;
import models.query.QPlayer;
import models.query.QShoutBox;
import models.query.QUser;
import models.query.QVote;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

public class GameInstanceApiController extends Controller {

	private static final String GAME_INSTANCE_ID = "game_instance_id";
	private static final String PLAYER = "player";
	private static final String PLAYER_ID = "player_id";
	private static final String MESSAGE = "message";
	private static final String VOTE = "vote";
	private static final String GAME = "game";
	private static final String GAME_ID= "game_id";
	private static final String PLACE= "place";
	private static final String DATE= "date";
	private static final String NAME = "name";
	
	public Result list() {
		
		QGameInstance gameQuery = new QGameInstance()
				.fetch("gameId");
				
		String name = request().getQueryString(NAME);
		if (name != null) {
			gameQuery.gameId.name.ilike(name + "%");
		}
			gameQuery.status.eq(GameInstanceStatus.CREATED.toString())
			.type.eq(GameInstanceType.PUBLIC.toString())
			.players.type.eq(PlayerType.HOST.toString())
			.players.userId.status.eq(UserStatus.ACTIVE.toString())
			.orderBy().date.asc();
		return ok(Json.toJson(gameQuery.findList()));
	}
	
	public Result getGame(Long id) {
		GameInstance game = new QGameInstance().id.eq(id).findUnique();
		if (game == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "bad_id"));
		}
		return ok(Json.toJson(game));
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public Result host() {
		JsonNode json = request().body().asJson();
		String username = json.findPath(UserApiController.USERNAME).textValue();
		String accessToken = json.findPath(UserApiController.ACCESS_TOKEN).textValue();
		String gameString = json.findPath(GAME_ID).textValue();
		String place = json.findPath(PLACE).textValue();
		String date = json.findPath(DATE).textValue();
		User user = new QUser().username.eq(username).accessToken.eq(accessToken).status.eq(UserStatus.ACTIVE.toString()).findUnique();
		if (user == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "no_access_token"));
		}
		if (gameString == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "game_not_found"));
		}
		Integer gameId = null;
		try {
			gameId = Integer.parseInt(gameString);
		} catch (NumberFormatException e) {
		}
		Game game = new QGame().id.eq(gameId).status.eq(GameStatus.ADDED.toString()).findUnique();
		if (game == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "game_not_found"));
		}
		GameInstance gameInstance = new GameInstance();
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");
		try {
			gameInstance.setDate(dateFormat.parse(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		gameInstance.setGame(game);
		gameInstance.setPlace(place);
		gameInstance.setStatus(GameInstanceStatus.CREATED);
		gameInstance.setType(GameInstanceType.PUBLIC);
		gameInstance.insert();
		Player player = new Player();
		player.setGameInstanceId(gameInstance);
		player.setUserId(user);
		player.setStatus(PlayerStatus.JOINED);
		player.setType(PlayerType.HOST);
		player.insert();
		return ok(Json.newObject().put(PLAYER, player.getStatus().toString()).put(GAME, "hosted"));
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public Result kick() {
		JsonNode json = request().body().asJson();
		String username = json.findPath(UserApiController.USERNAME).textValue();
		String accessToken = json.findPath(UserApiController.ACCESS_TOKEN).textValue();
		String playerIdString = json.findPath(PLAYER_ID).textValue();
		User user = new QUser().username.eq(username).accessToken.eq(accessToken).status.eq(UserStatus.ACTIVE.toString()).findUnique();
		if (user == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "no_access_token"));
		}
		Integer playerId = null;
		try {
			playerId = Integer.parseInt(playerIdString);
		} catch (NumberFormatException e) {
		}
		Player player = new QPlayer()
				.id.eq(playerId)
				.findUnique();
		if (player == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "player_not_found"));
		}
		List<ShoutBox> shouts = new QShoutBox().playerId.id.eq(player.getId()).findList();
		shouts.forEach(shout -> shout.delete());
		player.delete();
		return ok(Json.newObject().put(PLAYER, player.getStatus().toString()).put(GAME, "kicked"));
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public Result start() {
		JsonNode json = request().body().asJson();
		String username = json.findPath(UserApiController.USERNAME).textValue();
		String accessToken = json.findPath(UserApiController.ACCESS_TOKEN).textValue();
		String gameInstance = json.findPath(GAME_INSTANCE_ID).textValue();
		User user = new QUser().username.eq(username).accessToken.eq(accessToken).status.eq(UserStatus.ACTIVE.toString()).findUnique();
		if (user == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "no_access_token"));
		}
		if (gameInstance == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "game_not_found"));
		}
		Integer gameId = null;
		try {
			gameId = Integer.parseInt(gameInstance);
		} catch (NumberFormatException e) {
		}
		GameInstance game = new QGameInstance().id.eq(gameId).status.eq(GameInstanceStatus.CREATED.toString()).findUnique();
		if (game == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "game_not_found"));
		}
		Player player = new QPlayer()
				.gameInstanceId.id.eq(gameId)
				.userId.username.eq(username)
				.userId.accessToken.eq(accessToken)
				.findUnique();
		if (player == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "player_not_found"));
		}
		if (!player.getType().equals(PlayerType.HOST)) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "player_not_host"));
		}
		if (game.getPlayers().size() < game.getGame().getMinPlayers()) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "need_more_players"));
		}
		game.setStatus(GameInstanceStatus.STARTED);
		game.update();
		return ok(Json.newObject().put(PLAYER, player.getStatus().toString()).put(GAME, "started"));
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public Result finish() {
		JsonNode json = request().body().asJson();
		String username = json.findPath(UserApiController.USERNAME).textValue();
		String accessToken = json.findPath(UserApiController.ACCESS_TOKEN).textValue();
		String gameInstance = json.findPath(GAME_INSTANCE_ID).textValue();
		User user = new QUser().username.eq(username).accessToken.eq(accessToken).status.eq(UserStatus.ACTIVE.toString()).findUnique();
		if (user == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "no_access_token"));
		}
		if (gameInstance == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "game_not_found"));
		}
		Integer gameId = null;
		try {
			gameId = Integer.parseInt(gameInstance);
		} catch (NumberFormatException e) {
		}
		GameInstance game = new QGameInstance().id.eq(gameId).status.eq(GameInstanceStatus.STARTED.toString()).findUnique();
		if (game == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "game_not_found"));
		}
		Player player = new QPlayer()
				.gameInstanceId.id.eq(gameId)
				.userId.username.eq(username)
				.userId.accessToken.eq(accessToken)
				.findUnique();
		if (player == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "player_not_found"));
		}
		if (!player.getType().equals(PlayerType.HOST)) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "player_not_host"));
		}
		game.setStatus(GameInstanceStatus.FINISHED);
		game.update();
		return ok(Json.newObject().put(PLAYER, player.getStatus().toString()).put(GAME, "finished"));
	}

	
	@BodyParser.Of(BodyParser.Json.class)
	public Result join() {
		JsonNode json = request().body().asJson();
		String username = json.findPath(UserApiController.USERNAME).textValue();
		String accessToken = json.findPath(UserApiController.ACCESS_TOKEN).textValue();
		String gameInstanceId = json.findPath(GAME_INSTANCE_ID).textValue();
		User user = new QUser().username.eq(username).accessToken.eq(accessToken).status.eq(UserStatus.ACTIVE.toString()).findUnique();
		if (user == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "no_access_token"));
		}
		GameInstance gameInstance = null;
		try {
			gameInstance = new QGameInstance().id.eq(Integer.parseInt(gameInstanceId)).findUnique();
		} catch (NumberFormatException e) {
			
		}
		if (gameInstance == null || !gameInstance.getStatus().equals(GameInstanceStatus.CREATED)) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "bad_game_instance_id"));
		}
		Player checkPlayer = new QPlayer().userId.id.eq(user.getId()).and().gameInstanceId.id.eq(gameInstance.getId()).findUnique();
		if (checkPlayer != null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "already_joined"));
		}
		if (gameInstance.getPlayers().size() >= gameInstance.getGame().getMaxPlayers()) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "already_maximum_players"));
		}
		Player player = new Player();
		player.setGameInstanceId(gameInstance);
		player.setUserId(user);
		player.setStatus(PlayerStatus.JOINED);
		player.setType(PlayerType.PLAYER);
		player.insert();
		return ok(Json.newObject().put(PLAYER, player.getStatus().toString()));
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public Result leave() {
		JsonNode json = request().body().asJson();
		String username = json.findPath(UserApiController.USERNAME).textValue();
		String accessToken = json.findPath(UserApiController.ACCESS_TOKEN).textValue();
		String gameInstanceId = json.findPath(GAME_INSTANCE_ID).textValue();
		User user = new QUser().username.eq(username).accessToken.eq(accessToken).status.eq(UserStatus.ACTIVE.toString()).findUnique();
		if (user == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "no_access_token"));
		}
		GameInstance gameInstance = null;
		try {
			gameInstance = new QGameInstance().id.eq(Integer.parseInt(gameInstanceId)).findUnique();
		} catch (NumberFormatException e) {
			
		}
		if (gameInstance == null || !gameInstance.getStatus().equals(GameInstanceStatus.CREATED)) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "game_started"));
		}
		Player player = new QPlayer().userId.id.eq(user.getId()).and().gameInstanceId.id.eq(gameInstance.getId()).findUnique();
		if (player == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "not_in_game"));
		}
		if (player.getType().equals(PlayerType.PLAYER)) {
			Ebean.deleteAll(new QShoutBox().playerId.id.eq(player.getId()).findList());
			player.delete();
		} else {
			gameInstance.getPlayers().forEach(p -> { Ebean.deleteAll(new QShoutBox().playerId.id.eq(p.getId()).findList()); p.delete();});
			gameInstance.delete();
		}
		return ok(Json.newObject().put(PLAYER, "left"));
	}
	
	
	@BodyParser.Of(BodyParser.Json.class)
	public Result shout() {
		JsonNode json = request().body().asJson();
		String username = json.findPath(UserApiController.USERNAME).textValue();
		String accessToken = json.findPath(UserApiController.ACCESS_TOKEN).textValue();
		String gameInstanceId = json.findPath(GAME_INSTANCE_ID).textValue();
		String message = json.findPath(MESSAGE ).textValue();
		User user = new QUser().username.eq(username).accessToken.eq(accessToken).status.eq(UserStatus.ACTIVE.toString()).findUnique();
		if (user == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "no_access_token"));
		}
		GameInstance gameInstance = null;
		try {
			gameInstance = new QGameInstance().id.eq(Integer.parseInt(gameInstanceId)).findUnique();
		} catch (NumberFormatException e) {
			
		}
		if (gameInstance == null || !gameInstance.getStatus().equals(GameInstanceStatus.CREATED)) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "bad_game_instance_id"));
		}
		Player player = new QPlayer().userId.id.eq(user.getId()).and().gameInstanceId.id.eq(gameInstance.getId()).findUnique();
		if (player == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "not_in_the_game"));
		}
		if (message == null || "".equals(message)) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "no_message"));
		}
		ShoutBox shout = new ShoutBox();
		shout.setPlayerId(player);
		shout.setGameInstanceId(gameInstance);
		shout.setComment(message);
		shout.setCreatedOn(new Date());
		shout.insert();
		return ok(Json.newObject().put(MESSAGE, "sent"));
	}
	
	public Result getShouts(Long id) {
		List<ShoutBox> shout = new QShoutBox()
				.gameInstanceId.id.eq(id)
				.playerId.userId.status.eq(UserStatus.ACTIVE.toString())
				.order().createdOn.asc().findList();
		if (shout == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "no_shout"));
		}
		return ok(Json.toJson(shout));
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public Result vote() {
		JsonNode json = request().body().asJson();
		String username = json.findPath(UserApiController.USERNAME).textValue();
		String accessToken = json.findPath(UserApiController.ACCESS_TOKEN).textValue();
		String playerId = json.findPath(PLAYER_ID).textValue();
		String voteString = json.findPath(VOTE).textValue();
		User user = new QUser().username.eq(username).accessToken.eq(accessToken).status.eq(UserStatus.ACTIVE.toString()).findUnique();
		if (user == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "no_access_token"));
		}
		Player player = null;
		VoteType voteType = null;
		try {
			player = new QPlayer().id.eq(Integer.parseInt(playerId)).findUnique();
			voteType = VoteType.parse(Integer.parseInt(voteString));
		} catch (NumberFormatException e) {
		}
		if (player == null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "no_player"));
		}
		Vote voteCheck = new QVote().playerId.id.eq(player.getId()).userId.id.eq(user.getId()).findUnique();
		if (voteCheck != null) {
			return badRequest(Json.newObject().put(UserApiController.ERROR, "already_voted"));
		}
		Vote vote = new Vote();
		vote.setPlayerId(player);
		vote.setStatus(VoteStatus.VOTED);
		vote.setUserId(user);
		vote.setVote(voteType);
		vote.insert();
		if (voteType.getValue() == 1) {
			player.getUserId().setPositiveVotes(player.getUserId().getPositiveVotes() + 1);
		} else {
			player.getUserId().setNegativeVotes(player.getUserId().getNegativeVotes() + 1);
		}
		player.getUserId().update();
		return ok(Json.newObject().put(MESSAGE, "voted"));
	}
}
