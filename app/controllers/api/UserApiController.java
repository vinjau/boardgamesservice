package controllers.api;

import java.util.List;
import java.util.Random;

import org.apache.commons.codec.digest.DigestUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import akka.parboiled2.util.Base64;
import models.GameInstance;
import models.User;
import models.Vote;
import models.helpers.GameInstanceStatus;
import models.helpers.UserStatus;
import models.helpers.UserTypeEnum;
import models.query.QGameInstance;
import models.query.QUser;
import models.query.QUserType;
import models.query.QVote;
import play.Logger;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

public class UserApiController extends Controller {

	public static final String USERNAME = "username";
	private static final String PASSWORD = "password";
	private static final String EMAIL = "email";
	private static final String APP_TOKEN = "application_token";
	public static final String APP_TOKEN_CODE = "NAGhL6l8NMersxFAnI/qo1xpCMpUKvxtVdB78EdeGyE=";
	public static final String ACCESS_TOKEN = "access_token";
	public static final String ERROR = "error";
	public static final String GAME_INSTANCE_ID = "game_instance_id";
	
	// dodaj sliku
	@BodyParser.Of(BodyParser.Json.class)
	public Result register() {
		JsonNode json = request().body().asJson();
		String username = json.findPath(USERNAME).textValue();
		String password = json.findPath(PASSWORD).textValue();
		String email = json.findPath(EMAIL).textValue();
		String applicationToken = json.findPath(APP_TOKEN).textValue();
		if (applicationToken == null || !applicationToken.equals(APP_TOKEN_CODE)) {
			return badRequest();
		}
		User user = new User();
		user.setUsername(username);
		if (email == null || "".equals(email)) {
			user.setEmail(null);
		} else {
			user.setEmail(email);
		}
		user.setPassword(DigestUtils.sha256Hex(password));
		user.setStatus(UserStatus.ACTIVE);
		user.setAccessToken(createAccessToken());
		user.setUserType(new QUserType().type.eq(UserTypeEnum.USER.toString()).findUnique());
		user.setPositiveVotes(0);
		user.setNegativeVotes(0);
		User checkUser = new QUser().username.eq(username).findUnique();
		if (checkUser != null) {
			return badRequest(Json.newObject().put(ERROR, "username_exists"));
		}
		user.insert();
		Logger.info(username + " " + password + " " + email + " " + applicationToken);
		ObjectNode result = Json.newObject();
		result.put(ACCESS_TOKEN, user.getAccessToken());
		return ok(result);
	}
	
	private String createAccessToken() {
    	Random r = new Random();
    	byte[] b = new byte[32];
    	r.nextBytes(b);
    	return Base64.rfc2045().encodeToString(b, false);
	}
	
	public Result getUser() {
		String username = request().getQueryString(USERNAME);
		String accessToken = request().getQueryString(ACCESS_TOKEN);
		User user = new QUser().fetch("userTypeId").username.eq(username).accessToken.eq(accessToken).status.eq(UserStatus.ACTIVE.toString()).findUnique();
		if (user == null) {
			return badRequest(Json.newObject().put(ERROR, "no_user"));
		}
		return ok(Json.toJson(user));
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public Result login() {
		JsonNode json = request().body().asJson();
		String username = json.findPath(USERNAME).textValue();
		String password = json.findPath(PASSWORD).textValue();
		User user = new QUser().username.eq(username).password.eq(DigestUtils.sha256Hex(password)).status.eq(UserStatus.ACTIVE.toString()).findUnique();
		if (user == null) {
			return badRequest(Json.newObject().put(ERROR, "no_user"));
		}
		return ok(Json.newObject().put(ACCESS_TOKEN, user.getAccessToken()));
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public Result update() {
		JsonNode json = request().body().asJson();
		String username = json.findPath(USERNAME).textValue();
		String password = json.findPath(PASSWORD).textValue();
		String accessToken = json.findPath(UserApiController.ACCESS_TOKEN).textValue();
		String email = json.findPath(EMAIL).textValue();
		User user = new QUser().username.eq(username).accessToken.eq(accessToken).status.eq(UserStatus.ACTIVE.toString()).findUnique();
		if (user == null) {
			return badRequest(Json.newObject().put(ERROR, "no_user"));
		}
		if (password != null && !"".equals(password)) {
			user.setPassword(DigestUtils.sha256Hex(password));
			user.setAccessToken(createAccessToken());
			user.update();
			return ok(Json.newObject().put(ACCESS_TOKEN, user.getAccessToken()));
		}
		if (email != null && !"".equals(email)) {//email vec postoji check 
			User emailCheck = new QUser().email.eq(email).findUnique();
			if (emailCheck != null) {
				return badRequest(Json.newObject().put(ERROR, "email_exists"));
			}
			user.setEmail(email);
			user.update();
			return ok(Json.newObject().put(EMAIL, "changed"));
		}
		return ok(Json.newObject().put(ERROR, "no_change"));
	}
	
	public Result pastGames() {
		String username = request().getQueryString(USERNAME);
		String accessToken = request().getQueryString(ACCESS_TOKEN);
		List<GameInstance> games = new QGameInstance()
				.status.eq(GameInstanceStatus.FINISHED.toString())
				.players.userId.username.eq(username)
				.players.userId.accessToken.eq(accessToken)
				.players.userId.status.eq(UserStatus.ACTIVE.toString())
				.order().date.asc()
				.findList();
		return ok(Json.toJson(games));
	}
	
	public Result currentGames() {
		String username = request().getQueryString(USERNAME);
		String accessToken = request().getQueryString(ACCESS_TOKEN);
		List<GameInstance> games = new QGameInstance()
				.status.in(GameInstanceStatus.CREATED.toString(), GameInstanceStatus.STARTED.toString())
				.players.userId.username.eq(username)
				.players.userId.accessToken.eq(accessToken)
				.players.userId.status.eq(UserStatus.ACTIVE.toString())
				.order().date.asc()
				.findList();
		return ok(Json.toJson(games));
	}
	
	public Result getUserVotes() {
		String username = request().getQueryString(USERNAME);
		String accessToken = request().getQueryString(ACCESS_TOKEN);
		String gameInstanceIdString = request().getQueryString(GAME_INSTANCE_ID);
		Integer gameInstanceId = null;
		try {
			gameInstanceId = Integer.parseInt(gameInstanceIdString);
		} catch (Exception e) {
		}
		if (gameInstanceId == null) {
			return badRequest(Json.newObject().put(ERROR, "no_game_id"));
		}
		if (username == null || accessToken == null) {
			return badRequest(Json.newObject().put(ERROR, "no_user"));
		}
		List<Vote> votes = new QVote()
/*				.fetch("userId")
				.fetch("playerId gameInstanceId")*/
				.userId.accessToken.eq(accessToken)
				.userId.username.eq(username)
				.playerId.gameInstanceId.id.eq(gameInstanceId)
				.findList();
		return ok(Json.toJson(votes));
	}
}

