package generate;


import io.ebean.typequery.generator.Generator;
import io.ebean.typequery.generator.GeneratorConfig;

import java.io.IOException;

/**
 * Generate type query beans for each entity bean.
 */
public class MainQueryBeanGenerator {

  public static void main(String[] args) throws IOException {

    GeneratorConfig config = new GeneratorConfig();
    config.setClassesDirectory("target\\scala-2.12\\classes");
    config.setDestDirectory("app");
    config.setDestResourceDirectory("");
    
    config.setEntityBeanPackage("models");

    Generator generator = new Generator(config);
    generator.generateQueryBeans();
  }
}