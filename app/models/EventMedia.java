package models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.ebean.Model;
import models.helpers.MediaType;

@Entity
public class EventMedia extends Model {

	@Id
	private Long id;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "game_instance_id", nullable = false, updatable = false)
	private GameInstance gameInstanceId; // ako nesto ne radi preimenuj gameInstance ==> gameInstanceId
	
	private String media;
	
	@Enumerated(EnumType.STRING)
	private MediaType type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public GameInstance getGameInstance() {
		return gameInstanceId;
	}

	public void setGameInstance(GameInstance gameInstance) {
		this.gameInstanceId = gameInstance;
	}

	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	public MediaType getType() {
		return type;
	}

	public void setType(MediaType type) {
		this.type = type;
	}
}
