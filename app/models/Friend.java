package models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import io.ebean.Model;
import models.helpers.FriendStatus;

@Entity
public class Friend extends Model {

	@Id
	private Long id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user", nullable = false, updatable = false)
	private User userInitiatorId;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user", nullable = false, updatable = false)
	private User userFriendedId;
	
	@Enumerated(EnumType.STRING)
	private FriendStatus status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUserInitiatorId() {
		return userInitiatorId;
	}

	public void setUserInitiatorId(User userInitiatorId) {
		this.userInitiatorId = userInitiatorId;
	}

	public User getUserFriendedId() {
		return userFriendedId;
	}

	public void setUserFriendedId(User userFriendedId) {
		this.userFriendedId = userFriendedId;
	}

	public FriendStatus getStatus() {
		return status;
	}

	public void setStatus(FriendStatus status) {
		this.status = status;
	}
}
