package models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.ebean.Model;
import models.helpers.GameInstanceStatus;
import models.helpers.GameInstanceType;

@Entity
public class GameInstance extends Model {

	@Id
	private Long id;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "game_id", nullable = false, updatable = false)
	private Game gameId;
	
	private String place;
	
	@Enumerated(EnumType.STRING)
	private GameInstanceStatus status;

	@JsonIgnore // za sad
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "gameInstanceId", fetch = FetchType.LAZY)
	private List<EventMedia> eventMediaList;
	
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "gameInstanceId", fetch = FetchType.LAZY)
	private List<ShoutBox> shouts;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "gameInstanceId", fetch = FetchType.LAZY)
	private List<Player> players;
	
	@Enumerated(EnumType.STRING)
	private GameInstanceType type;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Game getGame() {
		return gameId;
	}

	public void setGame(Game game) {
		this.gameId = game;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public GameInstanceStatus getStatus() {
		return status;
	}

	public void setStatus(GameInstanceStatus status) {
		this.status = status;
	}
	
	public List<EventMedia> getEventMedia() {
		return eventMediaList;
	}

	public GameInstanceType getType() {
		return type;
	}

	public void setType(GameInstanceType type) {
		this.type = type;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public List<ShoutBox> getShouts() {
		return shouts;
	}
	
	public List<Player> getPlayers() {
		return players;
	}
	
}
