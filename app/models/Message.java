package models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import io.ebean.Model;
import models.helpers.MessageStatus;

@Entity
public class Message extends Model {

	@Id
	private Long id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_from_id", nullable = false, updatable = false)
	private User userFromId;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_to_id", nullable = false, updatable = false)
	private User userToId;
	
	private String message;
	
	@Enumerated(EnumType.STRING)
	private MessageStatus status;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUserFromId() {
		return userFromId;
	}

	public void setUserFromId(User userFromId) {
		this.userFromId = userFromId;
	}

	public User getUserToId() {
		return userToId;
	}

	public void setUserToId(User userToId) {
		this.userToId = userToId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public MessageStatus getStatus() {
		return status;
	}

	public void setStatus(MessageStatus status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
