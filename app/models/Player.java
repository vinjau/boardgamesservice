package models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.ebean.Model;
import models.helpers.PlayerStatus;
import models.helpers.PlayerType;

@Entity
public class Player extends Model {

	@Id
	private Long id;
	
	@JsonIgnore
	@ManyToOne(optional = false)
	@JoinColumn(name = "game_instance_id", updatable = false, nullable = false)
	private GameInstance gameInstanceId;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "user_id", updatable = false, nullable = false)
	private User userId;
	
	@Enumerated(EnumType.STRING)
	private PlayerStatus status;
	
	@Enumerated(EnumType.STRING)
	private PlayerType type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public GameInstance getGameInstanceId() {
		return gameInstanceId;
	}

	public void setGameInstanceId(GameInstance gameInstanceId) {
		this.gameInstanceId = gameInstanceId;
	}

	public User getUserId() {
		return userId;
	}

	public void setUserId(User userId) {
		this.userId = userId;
	}

	public PlayerStatus getStatus() {
		return status;
	}

	public void setStatus(PlayerStatus status) {
		this.status = status;
	}

	public PlayerType getType() {
		return type;
	}

	public void setType(PlayerType type) {
		this.type = type;
	}
}
