package models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.ebean.Model;

@Entity
public class ShoutBox extends Model {

	@Id
	private Long id;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "game_instance_id", nullable = false, updatable = false)
	private GameInstance gameInstanceId;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "player_id", nullable = false, updatable = false)
	private Player playerId;
	
	private String comment;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Player getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Player playerId) {
		this.playerId = playerId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public GameInstance getGameInstanceId() {
		return gameInstanceId;
	}

	public void setGameInstanceId(GameInstance gameInstanceid) {
		this.gameInstanceId = gameInstanceid;
	}
}
