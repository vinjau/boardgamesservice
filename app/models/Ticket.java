package models;

import javax.annotation.Nullable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import io.ebean.Model;
import models.helpers.TicketStatus;

@Entity
public class Ticket extends Model {

	@Id
	private Long id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user", nullable = false, updatable = false)
	private User userId;
	
	@Nullable
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user", nullable = true, updatable = false)
	private User userSupportId;
	
	@Enumerated(EnumType.STRING)
	private TicketStatus status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUserId() {
		return userId;
	}

	public void setUserId(User userId) {
		this.userId = userId;
	}

	public User getUserSupportId() {
		return userSupportId;
	}

	public void setUserSupportId(User userSupportId) {
		this.userSupportId = userSupportId;
	}

	public TicketStatus getStatus() {
		return status;
	}

	public void setStatus(TicketStatus status) {
		this.status = status;
	}
}
