package models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import io.ebean.Model;
import models.helpers.TicketSender;

@Entity
public class TicketMessage extends Model {

	@Id
	private Long id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ticket", nullable = false, updatable = false)
	private Ticket ticketMessageId;
	
	private String message;
	
	@Enumerated(EnumType.STRING)
	private TicketSender sender;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Ticket getTicketMessageId() {
		return ticketMessageId;
	}

	public void setTicketMessageId(Ticket ticketMessageId) {
		this.ticketMessageId = ticketMessageId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public TicketSender getSender() {
		return sender;
	}

	public void setSender(TicketSender sender) {
		this.sender = sender;
	}
}
