package models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.ebean.Model;
import models.helpers.UserStatus;

@Entity
public class User extends Model {

	@Id
	private Long id;
	
	// @Column(unique = true)
	private String username;

	@JsonIgnore
	private String password; // zabrani retrive iz baze - teska prica
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_type_id", nullable = false, updatable = false)
	private UserType userTypeId;

	@Enumerated(EnumType.STRING)
	private UserStatus status;
	
	// @Column(unique = true) Ebean ne podrzava
	private String email;
	
	private String picture;
	
	private Integer positiveVotes;
	
	private Integer negativeVotes;
	
	@JsonIgnore
	private String accessToken;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserType getUserType() {
		return userTypeId;
	}

	public void setUserType(UserType userType) {
		this.userTypeId = userType;
	}

	
	public UserStatus getStatus() {
		return status;
	}

	public void setStatus(UserStatus status) {
		this.status = status;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public Integer getPositiveVotes() {
		return positiveVotes;
	}

	public void setPositiveVotes(Integer positiveVotes) {
		this.positiveVotes = positiveVotes;
	}

	public Integer getNegativeVotes() {
		return negativeVotes;
	}

	public void setNegativeVotes(Integer negativeVotes) {
		this.negativeVotes = negativeVotes;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
}
