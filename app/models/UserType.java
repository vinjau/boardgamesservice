package models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import io.ebean.Model;
import models.helpers.UserTypeEnum;

@Entity
public class UserType extends Model {

	@Id
	private Long id;
	
	@Enumerated(EnumType.STRING)
	private UserTypeEnum type;
			
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserTypeEnum getType() {
		return type;
	}

	public void setType(UserTypeEnum type) {
		this.type = type;
	} 
}
