package models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import io.ebean.Model;
import models.helpers.VoteStatus;
import models.helpers.VoteType;

@Entity
public class Vote extends Model {

	@Id
	private Long id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", nullable = false, updatable = false)
	private User userId;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "player_id", nullable = false, updatable = false)
	private Player playerId;
	
	private int vote;
	
	@Enumerated(EnumType.STRING)
	private VoteStatus status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUserId() {
		return userId;
	}

	public void setUserId(User userId) {
		this.userId = userId;
	}

	public Player getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Player playerId) {
		this.playerId = playerId;
	}

	public VoteType getVote() {
		return VoteType.parse(vote);
	}

	public void setVote(VoteType voteType) {
		this.vote = voteType.getValue();
	}

	public VoteStatus getStatus() {
		return status;
	}

	public void setStatus(VoteStatus status) {
		this.status = status;
	}
}
