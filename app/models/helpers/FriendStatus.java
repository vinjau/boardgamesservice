package models.helpers;

public enum FriendStatus {

	REQUEST_PENDING, FRIENDS, BLOCKED, DELETED
}
