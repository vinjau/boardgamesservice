package models.helpers;

public enum GameInstanceStatus {

	CREATED, STARTED, FINISHED
}
