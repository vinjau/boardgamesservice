package models.helpers;

public enum MediaType {

	PICTURE, VIDEO
}
