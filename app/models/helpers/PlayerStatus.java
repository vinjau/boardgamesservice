package models.helpers;

public enum PlayerStatus {

	INVITED, JOINED
}
