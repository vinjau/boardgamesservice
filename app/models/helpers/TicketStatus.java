package models.helpers;

public enum TicketStatus {

	PENDING, ACTIVE, CLOSED, DELETED
}
