package models.helpers;

public enum UserStatus {
	
	ACTIVE, BANNED, DELETED
}
