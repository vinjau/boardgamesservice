package models.helpers;

public enum UserTypeEnum {

	ADMIN, MODERATOR, USER
}
