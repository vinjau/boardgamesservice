package models.helpers;

public enum VoteStatus {

	PENDING, VOTED
}
