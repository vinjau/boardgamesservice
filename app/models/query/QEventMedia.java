package models.query;

import io.ebean.EbeanServer;
import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.TQRootBean;
import io.ebean.typequery.TypeQueryBean;
import models.EventMedia;
import models.query.assoc.QAssocGameInstance;

/**
 * Query bean for EventMedia.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QEventMedia extends TQRootBean<EventMedia,QEventMedia> {

  private static final QEventMedia _alias = new QEventMedia(true);

  /**
   * Return the shared 'Alias' instance used to provide properties to 
   * <code>select()</code> and <code>fetch()</code> 
   */
  public static QEventMedia alias() {
    return _alias;
  }

  public PLong<QEventMedia> id;
  public QAssocGameInstance<QEventMedia> gameInstance;
  public PString<QEventMedia> media;
  public PString<QEventMedia> type;


  /**
   * Construct with a given EbeanServer.
   */
  public QEventMedia(EbeanServer server) {
    super(EventMedia.class, server);
  }

  /**
   * Construct using the default EbeanServer.
   */
  public QEventMedia() {
    super(EventMedia.class);
  }

  /**
   * Construct for Alias.
   */
  private QEventMedia(boolean dummy) {
    super(dummy);
  }
}
