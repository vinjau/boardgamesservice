package models.query;

import io.ebean.EbeanServer;
import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.TQRootBean;
import io.ebean.typequery.TypeQueryBean;
import models.Friend;
import models.query.assoc.QAssocUser;

/**
 * Query bean for Friend.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QFriend extends TQRootBean<Friend,QFriend> {

  private static final QFriend _alias = new QFriend(true);

  /**
   * Return the shared 'Alias' instance used to provide properties to 
   * <code>select()</code> and <code>fetch()</code> 
   */
  public static QFriend alias() {
    return _alias;
  }

  public PLong<QFriend> id;
  public QAssocUser<QFriend> userInitiatorId;
  public QAssocUser<QFriend> userFriendedId;
  public PString<QFriend> status;


  /**
   * Construct with a given EbeanServer.
   */
  public QFriend(EbeanServer server) {
    super(Friend.class, server);
  }

  /**
   * Construct using the default EbeanServer.
   */
  public QFriend() {
    super(Friend.class);
  }

  /**
   * Construct for Alias.
   */
  private QFriend(boolean dummy) {
    super(dummy);
  }
}
