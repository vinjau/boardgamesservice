package models.query;

import io.ebean.EbeanServer;
import io.ebean.typequery.PInteger;
import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.TQRootBean;
import io.ebean.typequery.TypeQueryBean;
import models.Game;
import models.query.assoc.QAssocGameMedia;

/**
 * Query bean for Game.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QGame extends TQRootBean<Game,QGame> {

  private static final QGame _alias = new QGame(true);

  /**
   * Return the shared 'Alias' instance used to provide properties to 
   * <code>select()</code> and <code>fetch()</code> 
   */
  public static QGame alias() {
    return _alias;
  }

  public PLong<QGame> id;
  public PString<QGame> name;
  public PString<QGame> description;
  public QAssocGameMedia<QGame> mediaSet;
  public PString<QGame> status;
  public PInteger<QGame> minPlayers;
  public PInteger<QGame> maxPlayers;

  /**
   * Construct with a given EbeanServer.
   */
  public QGame(EbeanServer server) {
    super(Game.class, server);
  }

  /**
   * Construct using the default EbeanServer.
   */
  public QGame() {
    super(Game.class);
  }

  /**
   * Construct for Alias.
   */
  private QGame(boolean dummy) {
    super(dummy);
  }
}
