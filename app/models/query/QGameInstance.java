package models.query;

import io.ebean.EbeanServer;
import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.PUtilDate;
import io.ebean.typequery.TQRootBean;
import io.ebean.typequery.TypeQueryBean;
import models.GameInstance;
import models.query.assoc.QAssocGame;
import models.query.assoc.QAssocPlayer;

/**
 * Query bean for GameInstance.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QGameInstance extends TQRootBean<GameInstance,QGameInstance> {

  private static final QGameInstance _alias = new QGameInstance(true);

  /**
   * Return the shared 'Alias' instance used to provide properties to 
   * <code>select()</code> and <code>fetch()</code> 
   */
  public static QGameInstance alias() {
    return _alias;
  }

  public PLong<QGameInstance> id;
  public QAssocGame<QGameInstance> gameId;
  public QAssocPlayer<QGameInstance> players;
  public PString<QGameInstance> place;
  public PString<QGameInstance> status;
  public PString<QGameInstance> type;
  public PUtilDate<QGameInstance> date;


  /**
   * Construct with a given EbeanServer.
   */
  public QGameInstance(EbeanServer server) {
    super(GameInstance.class, server);
  }

  /**
   * Construct using the default EbeanServer.
   */
  public QGameInstance() {
    super(GameInstance.class);
  }

  /**
   * Construct for Alias.
   */
  private QGameInstance(boolean dummy) {
    super(dummy);
  }
}
