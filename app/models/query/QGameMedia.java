package models.query;

import io.ebean.EbeanServer;
import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.TQRootBean;
import io.ebean.typequery.TypeQueryBean;
import models.GameMedia;
import models.query.assoc.QAssocGame;

/**
 * Query bean for GameMedia.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QGameMedia extends TQRootBean<GameMedia,QGameMedia> {

  private static final QGameMedia _alias = new QGameMedia(true);

  /**
   * Return the shared 'Alias' instance used to provide properties to 
   * <code>select()</code> and <code>fetch()</code> 
   */
  public static QGameMedia alias() {
    return _alias;
  }

  public PLong<QGameMedia> id;
  public QAssocGame<QGameMedia> game;
  public PString<QGameMedia> media;
  public PString<QGameMedia> type;


  /**
   * Construct with a given EbeanServer.
   */
  public QGameMedia(EbeanServer server) {
    super(GameMedia.class, server);
  }

  /**
   * Construct using the default EbeanServer.
   */
  public QGameMedia() {
    super(GameMedia.class);
  }

  /**
   * Construct for Alias.
   */
  private QGameMedia(boolean dummy) {
    super(dummy);
  }
}
