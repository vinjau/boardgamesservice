package models.query;

import io.ebean.EbeanServer;
import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.TQRootBean;
import io.ebean.typequery.TypeQueryBean;
import models.Player;
import models.query.assoc.QAssocGameInstance;
import models.query.assoc.QAssocUser;

/**
 * Query bean for Player.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QPlayer extends TQRootBean<Player,QPlayer> {

  private static final QPlayer _alias = new QPlayer(true);

  /**
   * Return the shared 'Alias' instance used to provide properties to 
   * <code>select()</code> and <code>fetch()</code> 
   */
  public static QPlayer alias() {
    return _alias;
  }

  public PLong<QPlayer> id;
  public QAssocGameInstance<QPlayer> gameInstanceId;
  public QAssocUser<QPlayer> userId;
  public PString<QPlayer> status;
  public PString<QPlayer> type;


  /**
   * Construct with a given EbeanServer.
   */
  public QPlayer(EbeanServer server) {
    super(Player.class, server);
  }

  /**
   * Construct using the default EbeanServer.
   */
  public QPlayer() {
    super(Player.class);
  }

  /**
   * Construct for Alias.
   */
  private QPlayer(boolean dummy) {
    super(dummy);
  }
}
