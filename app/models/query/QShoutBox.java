package models.query;

import io.ebean.EbeanServer;
import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.PUtilDate;
import io.ebean.typequery.TQRootBean;
import io.ebean.typequery.TypeQueryBean;
import models.ShoutBox;
import models.query.assoc.QAssocGameInstance;
import models.query.assoc.QAssocPlayer;

/**
 * Query bean for ShoutBox.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QShoutBox extends TQRootBean<ShoutBox,QShoutBox> {

  private static final QShoutBox _alias = new QShoutBox(true);

  /**
   * Return the shared 'Alias' instance used to provide properties to 
   * <code>select()</code> and <code>fetch()</code> 
   */
  public static QShoutBox alias() {
    return _alias;
  }

  public PLong<QShoutBox> id;
  public QAssocGameInstance<QShoutBox> gameInstanceId;
  public QAssocPlayer<QShoutBox> playerId;
  public PString<QShoutBox> comment;
  public PUtilDate<QShoutBox> createdOn;


  /**
   * Construct with a given EbeanServer.
   */
  public QShoutBox(EbeanServer server) {
    super(ShoutBox.class, server);
  }

  /**
   * Construct using the default EbeanServer.
   */
  public QShoutBox() {
    super(ShoutBox.class);
  }

  /**
   * Construct for Alias.
   */
  private QShoutBox(boolean dummy) {
    super(dummy);
  }
}
