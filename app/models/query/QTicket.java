package models.query;

import io.ebean.EbeanServer;
import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.TQRootBean;
import io.ebean.typequery.TypeQueryBean;
import models.Ticket;
import models.query.assoc.QAssocUser;

/**
 * Query bean for Ticket.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QTicket extends TQRootBean<Ticket,QTicket> {

  private static final QTicket _alias = new QTicket(true);

  /**
   * Return the shared 'Alias' instance used to provide properties to 
   * <code>select()</code> and <code>fetch()</code> 
   */
  public static QTicket alias() {
    return _alias;
  }

  public PLong<QTicket> id;
  public QAssocUser<QTicket> userId;
  public QAssocUser<QTicket> userSupportId;
  public PString<QTicket> status;


  /**
   * Construct with a given EbeanServer.
   */
  public QTicket(EbeanServer server) {
    super(Ticket.class, server);
  }

  /**
   * Construct using the default EbeanServer.
   */
  public QTicket() {
    super(Ticket.class);
  }

  /**
   * Construct for Alias.
   */
  private QTicket(boolean dummy) {
    super(dummy);
  }
}
