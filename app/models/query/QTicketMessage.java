package models.query;

import io.ebean.EbeanServer;
import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.TQRootBean;
import io.ebean.typequery.TypeQueryBean;
import models.TicketMessage;
import models.query.assoc.QAssocTicket;

/**
 * Query bean for TicketMessage.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QTicketMessage extends TQRootBean<TicketMessage,QTicketMessage> {

  private static final QTicketMessage _alias = new QTicketMessage(true);

  /**
   * Return the shared 'Alias' instance used to provide properties to 
   * <code>select()</code> and <code>fetch()</code> 
   */
  public static QTicketMessage alias() {
    return _alias;
  }

  public PLong<QTicketMessage> id;
  public QAssocTicket<QTicketMessage> ticketMessageId;
  public PString<QTicketMessage> message;
  public PString<QTicketMessage> sender;

  /**
   * Construct with a given EbeanServer.
   */
  public QTicketMessage(EbeanServer server) {
    super(TicketMessage.class, server);
  }

  /**
   * Construct using the default EbeanServer.
   */
  public QTicketMessage() {
    super(TicketMessage.class);
  }

  /**
   * Construct for Alias.
   */
  private QTicketMessage(boolean dummy) {
    super(dummy);
  }
}
