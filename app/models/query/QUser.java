package models.query;

import io.ebean.EbeanServer;
import io.ebean.typequery.PInteger;
import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.TQRootBean;
import io.ebean.typequery.TypeQueryBean;
import models.User;
import models.query.assoc.QAssocUserType;

/**
 * Query bean for User.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QUser extends TQRootBean<User,QUser> {

  private static final QUser _alias = new QUser(true);

  /**
   * Return the shared 'Alias' instance used to provide properties to 
   * <code>select()</code> and <code>fetch()</code> 
   */
  public static QUser alias() {
    return _alias;
  }

  public PLong<QUser> id;
  public PString<QUser> username;
  public PString<QUser> password;
  public QAssocUserType<QUser> userTypeId;
  public PString<QUser> email;
  public PString<QUser> picture;
  public PInteger<QUser> positiveVotes;
  public PInteger<QUser> negativeVotes;
  public PString<QUser> status;
  public PString<QUser> accessToken;


  /**
   * Construct with a given EbeanServer.
   */
  public QUser(EbeanServer server) {
    super(User.class, server);
  }

  /**
   * Construct using the default EbeanServer.
   */
  public QUser() {
    super(User.class);
  }

  /**
   * Construct for Alias.
   */
  private QUser(boolean dummy) {
    super(dummy);
  }
}
