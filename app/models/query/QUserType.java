package models.query;

import io.ebean.EbeanServer;
import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.TQRootBean;
import io.ebean.typequery.TypeQueryBean;
import models.UserType;

/**
 * Query bean for UserType.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QUserType extends TQRootBean<UserType,QUserType> {

  private static final QUserType _alias = new QUserType(true);

  /**
   * Return the shared 'Alias' instance used to provide properties to 
   * <code>select()</code> and <code>fetch()</code> 
   */
  public static QUserType alias() {
    return _alias;
  }

  public PLong<QUserType> id;
  public PString<QUserType> type;

  /**
   * Construct with a given EbeanServer.
   */
  public QUserType(EbeanServer server) {
    super(UserType.class, server);
  }

  /**
   * Construct using the default EbeanServer.
   */
  public QUserType() {
    super(UserType.class);
  }

  /**
   * Construct for Alias.
   */
  private QUserType(boolean dummy) {
    super(dummy);
  }
}
