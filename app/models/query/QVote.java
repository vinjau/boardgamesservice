package models.query;

import io.ebean.EbeanServer;
import io.ebean.typequery.PInteger;
import io.ebean.typequery.PLong;
import io.ebean.typequery.TQRootBean;
import io.ebean.typequery.TypeQueryBean;
import models.Vote;
import models.query.assoc.QAssocPlayer;
import models.query.assoc.QAssocUser;

/**
 * Query bean for Vote.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QVote extends TQRootBean<Vote,QVote> {

  private static final QVote _alias = new QVote(true);

  /**
   * Return the shared 'Alias' instance used to provide properties to 
   * <code>select()</code> and <code>fetch()</code> 
   */
  public static QVote alias() {
    return _alias;
  }

  public PLong<QVote> id;
  public QAssocUser<QVote> userId;
  public QAssocPlayer<QVote> playerId;
  public PInteger<QVote> vote;


  /**
   * Construct with a given EbeanServer.
   */
  public QVote(EbeanServer server) {
    super(Vote.class, server);
  }

  /**
   * Construct using the default EbeanServer.
   */
  public QVote() {
    super(Vote.class);
  }

  /**
   * Construct for Alias.
   */
  private QVote(boolean dummy) {
    super(dummy);
  }
}
