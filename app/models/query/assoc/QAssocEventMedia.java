package models.query.assoc;

import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.TQAssocBean;
import io.ebean.typequery.TQProperty;
import io.ebean.typequery.TypeQueryBean;
import models.EventMedia;
import models.query.QEventMedia;

/**
 * Association query bean for AssocEventMedia.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QAssocEventMedia<R> extends TQAssocBean<EventMedia,R> {

  public PLong<R> id;
  public QAssocGameInstance<R> gameInstance;
  public PString<R> media;
  public PString<R> type;

  /**
   * Eagerly fetch this association loading the specified properties.
   */
  @SafeVarargs
  public final R fetch(TQProperty<QEventMedia>... properties) {
    return fetchProperties(properties);
  }

  public QAssocEventMedia(String name, R root) {
    super(name, root);
  }
}
