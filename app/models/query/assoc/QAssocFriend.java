package models.query.assoc;

import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.TQAssocBean;
import io.ebean.typequery.TQProperty;
import io.ebean.typequery.TypeQueryBean;
import models.Friend;
import models.query.QFriend;

/**
 * Association query bean for AssocFriend.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QAssocFriend<R> extends TQAssocBean<Friend,R> {

  public PLong<R> id;
  public QAssocUser<R> userInitiatorId;
  public QAssocUser<R> userFriendedId;
  public PString<R> status;

  /**
   * Eagerly fetch this association loading the specified properties.
   */
  @SafeVarargs
  public final R fetch(TQProperty<QFriend>... properties) {
    return fetchProperties(properties);
  }

  public QAssocFriend(String name, R root) {
    super(name, root);
  }
}
