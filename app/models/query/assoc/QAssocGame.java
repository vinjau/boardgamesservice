package models.query.assoc;

import io.ebean.typequery.PInteger;
import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.TQAssocBean;
import io.ebean.typequery.TQProperty;
import io.ebean.typequery.TypeQueryBean;
import models.Game;
import models.query.QGame;

/**
 * Association query bean for AssocGame.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QAssocGame<R> extends TQAssocBean<Game,R> {

  public PLong<R> id;
  public PString<R> name;
  public PString<R> description;
  public QAssocGameMedia<R> mediaSet;
  public PString<R> status;
  public PInteger<R> minPlayers;
  public PInteger<R> maxPlayers;

  /**
   * Eagerly fetch this association loading the specified properties.
   */
  @SafeVarargs
  public final R fetch(TQProperty<QGame>... properties) {
    return fetchProperties(properties);
  }

  public QAssocGame(String name, R root) {
    super(name, root);
  }
}
