package models.query.assoc;

import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.PUtilDate;
import io.ebean.typequery.TQAssocBean;
import io.ebean.typequery.TQProperty;
import io.ebean.typequery.TypeQueryBean;
import models.GameInstance;
import models.query.QGameInstance;

/**
 * Association query bean for AssocGameInstance.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QAssocGameInstance<R> extends TQAssocBean<GameInstance,R> {

  public PLong<R> id;
  public QAssocGame<R> gameId;
  public QAssocPlayer<R> players;
  public PString<R> place;
  public PString<R> status;
  public PString<R> type;
  public PUtilDate<R> date;

  /**
   * Eagerly fetch this association loading the specified properties.
   */
  @SafeVarargs
  public final R fetch(TQProperty<QGameInstance>... properties) {
    return fetchProperties(properties);
  }

  public QAssocGameInstance(String name, R root) {
    super(name, root);
  }
}
