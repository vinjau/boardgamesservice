package models.query.assoc;

import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.TQAssocBean;
import io.ebean.typequery.TQProperty;
import io.ebean.typequery.TypeQueryBean;
import models.GameMedia;
import models.query.QGameMedia;

/**
 * Association query bean for AssocGameMedia.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QAssocGameMedia<R> extends TQAssocBean<GameMedia,R> {

  public PLong<R> id;
  public QAssocGame<R> game;
  public PString<R> media;
  public PString<R> type;

  /**
   * Eagerly fetch this association loading the specified properties.
   */
  @SafeVarargs
  public final R fetch(TQProperty<QGameMedia>... properties) {
    return fetchProperties(properties);
  }

  public QAssocGameMedia(String name, R root) {
    super(name, root);
  }
}
