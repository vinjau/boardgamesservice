package models.query.assoc;

import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.PUtilDate;
import io.ebean.typequery.TQAssocBean;
import io.ebean.typequery.TQProperty;
import io.ebean.typequery.TypeQueryBean;
import models.Message;
import models.query.QMessage;

/**
 * Association query bean for AssocMessage.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QAssocMessage<R> extends TQAssocBean<Message,R> {

  public PLong<R> id;
  public QAssocUser<R> userFromId;
  public QAssocUser<R> userToId;
  public PString<R> message;
  public PString<R> status;
  public PUtilDate<R> createdOn;

  /**
   * Eagerly fetch this association loading the specified properties.
   */
  @SafeVarargs
  public final R fetch(TQProperty<QMessage>... properties) {
    return fetchProperties(properties);
  }

  public QAssocMessage(String name, R root) {
    super(name, root);
  }
}
