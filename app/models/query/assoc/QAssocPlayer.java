package models.query.assoc;

import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.TQAssocBean;
import io.ebean.typequery.TQProperty;
import io.ebean.typequery.TypeQueryBean;
import models.Player;
import models.query.QPlayer;

/**
 * Association query bean for AssocPlayer.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QAssocPlayer<R> extends TQAssocBean<Player,R> {

  public PLong<R> id;
  public QAssocGameInstance<R> gameInstanceId;
  public QAssocUser<R> userId;
  public PString<R> status;
  public PString<R> type;

  /**
   * Eagerly fetch this association loading the specified properties.
   */
  @SafeVarargs
  public final R fetch(TQProperty<QPlayer>... properties) {
    return fetchProperties(properties);
  }

  public QAssocPlayer(String name, R root) {
    super(name, root);
  }
}
