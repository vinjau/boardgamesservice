package models.query.assoc;

import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.PUtilDate;
import io.ebean.typequery.TQAssocBean;
import io.ebean.typequery.TQProperty;
import io.ebean.typequery.TypeQueryBean;
import models.ShoutBox;
import models.query.QShoutBox;

/**
 * Association query bean for AssocShoutBox.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QAssocShoutBox<R> extends TQAssocBean<ShoutBox,R> {

  public PLong<R> id;
  public QAssocGameInstance<R> gameInstanceId;
  public QAssocPlayer<R> playerId;
  public PString<R> comment;
  public PUtilDate<R> createdOn;

  /**
   * Eagerly fetch this association loading the specified properties.
   */
  @SafeVarargs
  public final R fetch(TQProperty<QShoutBox>... properties) {
    return fetchProperties(properties);
  }

  public QAssocShoutBox(String name, R root) {
    super(name, root);
  }
}
