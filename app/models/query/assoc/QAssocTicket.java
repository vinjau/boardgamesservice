package models.query.assoc;

import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.TQAssocBean;
import io.ebean.typequery.TQProperty;
import io.ebean.typequery.TypeQueryBean;
import models.Ticket;
import models.query.QTicket;

/**
 * Association query bean for AssocTicket.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QAssocTicket<R> extends TQAssocBean<Ticket,R> {

  public PLong<R> id;
  public QAssocUser<R> userId;
  public QAssocUser<R> userSupportId;
  public PString<R> status;

  /**
   * Eagerly fetch this association loading the specified properties.
   */
  @SafeVarargs
  public final R fetch(TQProperty<QTicket>... properties) {
    return fetchProperties(properties);
  }

  public QAssocTicket(String name, R root) {
    super(name, root);
  }
}
