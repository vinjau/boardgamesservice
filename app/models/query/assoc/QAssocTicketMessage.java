package models.query.assoc;

import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.TQAssocBean;
import io.ebean.typequery.TQProperty;
import io.ebean.typequery.TypeQueryBean;
import models.TicketMessage;
import models.query.QTicketMessage;

/**
 * Association query bean for AssocTicketMessage.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QAssocTicketMessage<R> extends TQAssocBean<TicketMessage,R> {

  public PLong<R> id;
  public QAssocTicket<R> ticketMessageId;
  public PString<R> message;
  public PString<R> sender;

  /**
   * Eagerly fetch this association loading the specified properties.
   */
  @SafeVarargs
  public final R fetch(TQProperty<QTicketMessage>... properties) {
    return fetchProperties(properties);
  }

  public QAssocTicketMessage(String name, R root) {
    super(name, root);
  }
}
