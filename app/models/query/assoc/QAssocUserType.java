package models.query.assoc;

import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.TQAssocBean;
import io.ebean.typequery.TQProperty;
import io.ebean.typequery.TypeQueryBean;
import models.UserType;
import models.query.QUserType;

/**
 * Association query bean for AssocUserType.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QAssocUserType<R> extends TQAssocBean<UserType,R> {

  public PLong<R> id;
  public PString<R> type;

  /**
   * Eagerly fetch this association loading the specified properties.
   */
  @SafeVarargs
  public final R fetch(TQProperty<QUserType>... properties) {
    return fetchProperties(properties);
  }

  public QAssocUserType(String name, R root) {
    super(name, root);
  }
}
