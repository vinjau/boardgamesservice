package models.query.assoc;

import io.ebean.typequery.PInteger;
import io.ebean.typequery.PLong;
import io.ebean.typequery.TQAssocBean;
import io.ebean.typequery.TQProperty;
import io.ebean.typequery.TypeQueryBean;
import models.Vote;
import models.query.QVote;

/**
 * Association query bean for AssocVote.
 * 
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QAssocVote<R> extends TQAssocBean<Vote,R> {

  public PLong<R> id;
  public QAssocUser<R> userId;
  public QAssocPlayer<R> playerId;
  public PInteger<R> vote;

  /**
   * Eagerly fetch this association loading the specified properties.
   */
  @SafeVarargs
  public final R fetch(TQProperty<QVote>... properties) {
    return fetchProperties(properties);
  }

  public QAssocVote(String name, R root) {
    super(name, root);
  }
}
