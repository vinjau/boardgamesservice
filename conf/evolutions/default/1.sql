# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table event_media (
  id                            bigint auto_increment not null,
  game_instance_id              bigint not null,
  media                         varchar(255),
  type                          varchar(7),
  constraint ck_event_media_type check ( type in ('PICTURE','VIDEO')),
  constraint pk_event_media primary key (id)
);

create table friend (
  id                            bigint auto_increment not null,
  user                          bigint not null,
  status                        varchar(15),
  constraint ck_friend_status check ( status in ('REQUEST_PENDING','FRIENDS','BLOCKED','DELETED')),
  constraint pk_friend primary key (id)
);

create table game (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  description                   varchar(255),
  min_players                   integer not null,
  max_players                   integer not null,
  status                        varchar(7),
  constraint ck_game_status check ( status in ('PENDING','ADDED')),
  constraint pk_game primary key (id)
);

create table game_instance (
  id                            bigint auto_increment not null,
  game_id                       bigint not null,
  place                         varchar(255),
  status                        varchar(8),
  type                          varchar(7),
  date                          datetime(6),
  constraint ck_game_instance_status check ( status in ('CREATED','STARTED','FINISHED')),
  constraint ck_game_instance_type check ( type in ('PRIVATE','PUBLIC')),
  constraint pk_game_instance primary key (id)
);

create table game_media (
  id                            bigint auto_increment not null,
  game_id                       bigint not null,
  media                         varchar(255),
  type                          varchar(7),
  constraint ck_game_media_type check ( type in ('PICTURE','VIDEO')),
  constraint pk_game_media primary key (id)
);

create table message (
  id                            bigint auto_increment not null,
  user_from_id                  bigint not null,
  user_to_id                    bigint not null,
  message                       varchar(255),
  status                        varchar(8),
  created_on                    datetime(6),
  constraint ck_message_status check ( status in ('PENDING','SENT','RECIEVED','SEEN')),
  constraint pk_message primary key (id)
);

create table player (
  id                            bigint auto_increment not null,
  game_instance_id              bigint not null,
  user_id                       bigint not null,
  status                        varchar(7),
  type                          varchar(6),
  constraint ck_player_status check ( status in ('INVITED','JOINED')),
  constraint ck_player_type check ( type in ('HOST','PLAYER')),
  constraint pk_player primary key (id)
);

create table shout_box (
  id                            bigint auto_increment not null,
  game_instance_id              bigint not null,
  player_id                     bigint not null,
  comment                       varchar(255),
  created_on                    datetime(6),
  constraint pk_shout_box primary key (id)
);

create table ticket (
  id                            bigint auto_increment not null,
  user                          bigint not null,
  status                        varchar(7),
  constraint ck_ticket_status check ( status in ('PENDING','ACTIVE','CLOSED','DELETED')),
  constraint pk_ticket primary key (id)
);

create table ticket_message (
  id                            bigint auto_increment not null,
  ticket                        bigint not null,
  message                       varchar(255),
  sender                        varchar(7),
  constraint ck_ticket_message_sender check ( sender in ('USER','SUPPORT')),
  constraint pk_ticket_message primary key (id)
);

create table user (
  id                            bigint auto_increment not null,
  username                      varchar(255),
  password                      varchar(255),
  user_type_id                  bigint not null,
  status                        varchar(7),
  email                         varchar(255),
  picture                       varchar(255),
  positive_votes                integer,
  negative_votes                integer,
  access_token                  varchar(255),
  constraint ck_user_status check ( status in ('ACTIVE','BANNED','DELETED')),
  constraint pk_user primary key (id)
);

create table user_type (
  id                            bigint auto_increment not null,
  type                          varchar(9),
  constraint ck_user_type_type check ( type in ('ADMIN','MODERATOR','USER')),
  constraint pk_user_type primary key (id)
);

create table vote (
  id                            bigint auto_increment not null,
  user_id                       bigint not null,
  player_id                     bigint not null,
  vote                          integer not null,
  status                        varchar(7),
  constraint ck_vote_status check ( status in ('PENDING','VOTED')),
  constraint pk_vote primary key (id)
);

alter table event_media add constraint fk_event_media_game_instance_id foreign key (game_instance_id) references game_instance (id) on delete restrict on update restrict;
create index ix_event_media_game_instance_id on event_media (game_instance_id);

alter table friend add constraint fk_friend_user foreign key (user) references user (id) on delete restrict on update restrict;
create index ix_friend_user on friend (user);

alter table game_instance add constraint fk_game_instance_game_id foreign key (game_id) references game (id) on delete restrict on update restrict;
create index ix_game_instance_game_id on game_instance (game_id);

alter table game_media add constraint fk_game_media_game_id foreign key (game_id) references game (id) on delete restrict on update restrict;
create index ix_game_media_game_id on game_media (game_id);

alter table message add constraint fk_message_user_from_id foreign key (user_from_id) references user (id) on delete restrict on update restrict;
create index ix_message_user_from_id on message (user_from_id);

alter table message add constraint fk_message_user_to_id foreign key (user_to_id) references user (id) on delete restrict on update restrict;
create index ix_message_user_to_id on message (user_to_id);

alter table player add constraint fk_player_game_instance_id foreign key (game_instance_id) references game_instance (id) on delete restrict on update restrict;
create index ix_player_game_instance_id on player (game_instance_id);

alter table player add constraint fk_player_user_id foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_player_user_id on player (user_id);

alter table shout_box add constraint fk_shout_box_game_instance_id foreign key (game_instance_id) references game_instance (id) on delete restrict on update restrict;
create index ix_shout_box_game_instance_id on shout_box (game_instance_id);

alter table shout_box add constraint fk_shout_box_player_id foreign key (player_id) references player (id) on delete restrict on update restrict;
create index ix_shout_box_player_id on shout_box (player_id);

alter table ticket add constraint fk_ticket_user foreign key (user) references user (id) on delete restrict on update restrict;
create index ix_ticket_user on ticket (user);

alter table ticket_message add constraint fk_ticket_message_ticket foreign key (ticket) references ticket (id) on delete restrict on update restrict;
create index ix_ticket_message_ticket on ticket_message (ticket);

alter table user add constraint fk_user_user_type_id foreign key (user_type_id) references user_type (id) on delete restrict on update restrict;
create index ix_user_user_type_id on user (user_type_id);

alter table vote add constraint fk_vote_user_id foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_vote_user_id on vote (user_id);

alter table vote add constraint fk_vote_player_id foreign key (player_id) references player (id) on delete restrict on update restrict;
create index ix_vote_player_id on vote (player_id);


# --- !Downs

alter table event_media drop foreign key fk_event_media_game_instance_id;
drop index ix_event_media_game_instance_id on event_media;

alter table friend drop foreign key fk_friend_user;
drop index ix_friend_user on friend;

alter table game_instance drop foreign key fk_game_instance_game_id;
drop index ix_game_instance_game_id on game_instance;

alter table game_media drop foreign key fk_game_media_game_id;
drop index ix_game_media_game_id on game_media;

alter table message drop foreign key fk_message_user_from_id;
drop index ix_message_user_from_id on message;

alter table message drop foreign key fk_message_user_to_id;
drop index ix_message_user_to_id on message;

alter table player drop foreign key fk_player_game_instance_id;
drop index ix_player_game_instance_id on player;

alter table player drop foreign key fk_player_user_id;
drop index ix_player_user_id on player;

alter table shout_box drop foreign key fk_shout_box_game_instance_id;
drop index ix_shout_box_game_instance_id on shout_box;

alter table shout_box drop foreign key fk_shout_box_player_id;
drop index ix_shout_box_player_id on shout_box;

alter table ticket drop foreign key fk_ticket_user;
drop index ix_ticket_user on ticket;

alter table ticket_message drop foreign key fk_ticket_message_ticket;
drop index ix_ticket_message_ticket on ticket_message;

alter table user drop foreign key fk_user_user_type_id;
drop index ix_user_user_type_id on user;

alter table vote drop foreign key fk_vote_user_id;
drop index ix_vote_user_id on vote;

alter table vote drop foreign key fk_vote_player_id;
drop index ix_vote_player_id on vote;

drop table if exists event_media;

drop table if exists friend;

drop table if exists game;

drop table if exists game_instance;

drop table if exists game_media;

drop table if exists message;

drop table if exists player;

drop table if exists shout_box;

drop table if exists ticket;

drop table if exists ticket_message;

drop table if exists user;

drop table if exists user_type;

drop table if exists vote;

